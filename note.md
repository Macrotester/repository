# 软件测试学习笔记


#目录




**软件 = 程序 + 数据 + 文档**




##基本结构
-用户端
-服务端


###域名
由一串用点分隔的字符组成的互联网上某一台计算机或计算机组的名称，用于在数据传输时标识计算机的电子方位。

###端口 port
65535

###ip
-本机IP：127.0.0.1（localhost）
-局域网（私网）：ipconfig
-互联网（公网）：百度IP


SELECT AVG(english) from mark;

SELECT math,name,address,Telno from mark join cust on mark.studentno=cust.Studentno where mark.studentno in (11,22,33,44,55);

SELECT name,computer from mark join cust on mark.studentno=cust.Studentno ORDER BY computer DESC;

SELECT mark.studentno,name,(math+english+computer) total from mark join cust on mark.studentno=cust.Studentno where (math+english+computer)>240 ORDER BY total DESC;
